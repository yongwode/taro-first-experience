import { Component } from 'react'
import { Provider } from 'mobx-react'
import Taro from "@tarojs/taro"
import counterStore from './store/counter'
import filmStore from './store/film'

import './app.less'

const store = {
    counterStore,
    filmStore
}

class App extends Component {
    componentDidMount() { }
    config = {
        pages: [
            'pages/index/index',
            'pages/login/Login',
        ],
        window: {
            backgroundTextStyle: 'light',
            navigationBarBackgroundColor: '#fff',
            navigationBarTitleText: 'WeChat',
            navigationBarTextStyle: 'black'
        },
        tabBar: {
            list: [
                {
                    pagePath: "pages/index/index",
                    text: "首页",
                },
                {
                    pagePath: 'pages/login/Login',
                    text: "登录",
                }
                


            ]
        }
    }
    componentDidShow() { }

    componentDidHide() { }

    componentDidCatchError() { }

    // this.props.children 就是要渲染的页面
    render() {
        return (
            <Provider store={store}>
                {this.props.children}
            </Provider>
        )
    }
}

export default App
