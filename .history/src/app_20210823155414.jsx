
import 'taro-ui/dist/style/index.scss'
import { Component } from 'react';
import { Provider } from 'mobx-react';
import counterStore from './store/counter';
import filmStore from './store/film';
import Taro, { Config } from "@tarojs/taro"

import './app.less';

const store = {
    counterStore,
    filmStore
}

class App extends Component {
    componentDidMount() {

        Taro.setNavigationBarColor({
            frontColor: '#000000',
            backgroundColor: '#000000',
            animation: {
                duration: 30,
                timingFunc: 'linear'
            }
        });
    }

    componentDidShow() { }

    componentDidHide() { }

    componentDidCatchError() { }

    // this.props.children 就是要渲染的页面
    render() {
        return (
            <Provider store={store}>
                {this.props.children}
            </Provider>
        )
    }
}

export default App
