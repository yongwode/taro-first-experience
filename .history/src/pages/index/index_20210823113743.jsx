import { useEffect, useState } from 'react'
import { View, Button, Text } from '@tarojs/components'
import Taro from "@tarojs/taro"
import { AtButton, AtForm, AtInput } from 'taro-ui'
import './index.less'

const Index = (props) => {
    const [uesr, setUser] = useState(0)
    const [password, setPasswod] = useState(0)
    useEffect(() => {
        console.log(props);
    }, [])
    const onSubmit = (e) => {
        console.log(e);
    }
    const onReset = (e) => {
        console.log(e);
    }
    const handleChange = (value, e) => {
        console.log(value);
        console.log(e);
    }
    return (
        <View className='index'>
            <AtForm
                onSubmit={onSubmit.bind(this)}
                onReset={onReset.bind(this)}
            >
                <AtInput
                    name='username'
                    title='账号'
                    type='text'
                    placeholder='请输入账号'
                    value={uesr}
                    onChange={handleChange.bind(this, 'username')}
                />
                <AtInput
                    name='password'
                    title='密码'
                    type='text'
                    placeholder='请输入密码'
                    value={password}
                    onChange={handleChange.bind(this, 'value')}
                />
                <AtButton onClick={() => {
                    Taro.navigateTo({
                        url: "../login/Login"
                    })
                }} formType='submit'>提交</AtButton>
            </AtForm>



            {/* 
        <Button onClick={decrement}>-</Button>
        <Button onClick={incrementAsync}>Add Async</Button>
        <Text>{num}</Text> */}
            {/* <Login text='登录'></Login> */}
            {/* <List data={filmStore}></List> */}

        </View>
    )
}
export default Index
