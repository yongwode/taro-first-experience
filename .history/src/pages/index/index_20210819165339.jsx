import React, { Component,useEffect,useState } from 'react'

import { View, Button, Text } from '@tarojs/components'
import { observer, inject } from 'mobx-react'
import Login from "../../components/login/index"
import List from "../../components/list/List"
import './index.less'
import filmStore from "../../store/film"
const Index = (props)=>{
  const [num ,setNum] = useState(0)
useEffect(()=>{
    console.log(props);
},[])
  const increment=()=>{
    setNum(num+1)
  }
  const decrement=()=>{
    setNum(num-1)
  }
  const incrementAsync=()=>{
    setNum(0)
  }
  return(
    <View className='index'>
        <Button onClick={increment}>+</Button>
        <Button onClick={decrement}>-</Button>
        <Button onClick={incrementAsync}>Add Async</Button>
        {/* <Text>{num}</Text> */}
        {/* <Login text='登录'></Login> */}
        <List data={filmStore}></List>

      </View>
  )
}
export default Index
