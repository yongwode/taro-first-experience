import { useEffect, useState } from 'react'
import { View, Button, Text } from '@tarojs/components'
import Taro from "@tarojs/taro"
import { AtButton, AtForm, AtInput } from 'taro-ui'
import './index.less'

const Index = (props) => {
  useEffect(() => {
    console.log(props);
	Taro.setNavigationBarColor({
		backgroundColor:'16进制颜色',
		frontColor:'#ffffff / #000000',	 //前景颜色值，包括按钮、标题、状态栏的颜色
		// animation:{
		// 	duration:'动画时间',
		// 	timingFunc: easeIn  //'linear'、'easeIn'、'easeOut'、'easeInOut'
		// } "usingComponents": {},
  "navigationStyle": "custom"
	})
    
  }, [])
  
  return (
    <View className='index'>
     


      <Button className="goLogin" onClick={() => {
        Taro.navigateTo({
          url: "../login/Login"
        })
      }}>登录</Button>

      {/* 
        <Button onClick={decrement}>-</Button>
        <Button onClick={incrementAsync}>Add Async</Button>
        <Text>{num}</Text> */}
      {/* <Login text='登录'></Login> */}
      {/* <List data={filmStore}></List> */}

    </View>
  )
}
export default Index
