import React, { Component,useEffect,useState } from 'react'

import { View, Button, Text } from '@tarojs/components'
import { observer, inject } from 'mobx-react'

import './index.less'
const Index = (props)=>{
useEffect(()=>{
    console.log(props);
},[])
  const increment=()=>{

  }
  const decrement=()=>{
    
  }
  const incrementAsync=()=>{
    
  }
  return(
    <View className='index'>
        <Button onClick={increment}>+</Button>
        <Button onClick={decrement}>-</Button>
        <Button onClick={incrementAsync}>Add Async</Button>
        <Text>222</Text>
      </View>
  )
}
export default Index
