import React, { Component, useEffect, useState } from 'react'
import Taro, { Config } from "@tarojs/taro"
import { AtButton, AtForm, AtInput } from 'taro-ui'
import { View, Button, Text } from '@tarojs/components'
import { observer, inject } from 'mobx-react'
import './index.less'
const Login = (props) => {
  const [uesr, setUser] = useState("")
  const [password, setPasswod] = useState("")

    const { text } = props
    const [num, setNum] = useState(0)
    useEffect(() => {
        //设置标题
        Taro.setNavigationBarTitle({
            title: "登录"
        })
        console.log(props);
    }, [])
    const onSubmit = (e) => {
        console.log(e);
      }
      const onReset = (e) => {
        console.log(e);
      }
      const handleChange = (value,e) => {
        console.log(value);
        console.log(e);
      }
    return (
        <View className='login'>
            <View className="from">
                <AtForm
                    onSubmit={onSubmit.bind(this)}
                    onReset={onReset.bind(this)}
                >
                    <AtInput
                    className="login_input"
                        name='username'
                        title='账号'
                        type='text'
                        placeholder='请输入账号'
                        value={uesr}
                        onChange={handleChange.bind(this, 'username')}
                    />
                    <AtInput
                        name='password'
                        title='密码'
                        type='text'
                        placeholder='请输入密码'
                        value={password}
                        onChange={handleChange.bind(this, 'value')}
                    />
                    <AtButton formType='submit'>登录</AtButton>
                </AtForm>
            </View>
        </View>
    )
}
export default Login
