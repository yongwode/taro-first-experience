import React, { Component, useEffect, useState } from 'react'
import Taro, { Config } from "@tarojs/taro"
import { View, Button, Text } from '@tarojs/components'
import { observer, inject } from 'mobx-react'
import Logo from '../../assets/images/00.png'
import './index.less'
const Login = (props) => {
    const { text } = props
    const [num, setNum] = useState(0)
    useEffect(() => {
        //设置标题
        Taro.setNavigationBarTitle({
            title: "易简教学督导平台"
        })
        console.log(props);
    }, [])

    return (
        <View className='login'>
            <View className="logo">
            <image className="logo_img" src={Logo} alt="" />
            </View>
            <View>登录界面</View>
        </View>
    )
}
export default Login
