import React, { Component, useEffect, useState } from 'react'
import Taro, { Config } from "@tarojs/taro"
import { View, Button, Text } from '@tarojs/components'
import { observer, inject } from 'mobx-react'
import './index.less'
const Login = (props) => {
    const { text } = props
    const [num, setNum] = useState(0)
    useEffect(() => {
        //设置标题
        Taro.setNavigationBarTitle({
            title: "登录"
        })
        console.log(props);
    }, [])

    return (
        <View className='index'>
            <View>登录界面</View>
            <View>{text}</View>
        </View>
    )
}
export default Login
