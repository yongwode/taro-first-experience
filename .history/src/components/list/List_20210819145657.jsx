import React, { Component, useEffect, useState } from 'react'
import { View, Button, Text } from '@tarojs/components'
const List = (props) => {
    const { films } = props
    useEffect(() => {
        console.log(films);
    }, [])

    return (
        <View className='list'>
            <View>列表</View>
            {
                films.map((item,index)=>{
                        <View key={index}>
                            {item.title}
                        </View>
                })
            }

        </View>
    )
}
export default List
