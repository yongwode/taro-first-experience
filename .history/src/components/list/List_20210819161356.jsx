import React, { Component, useEffect, useState } from 'react'
import { View, Button, Text,Picker } from '@tarojs/components'
const List = (props) => {
    const { films, getfilms } = props.data
    const [list, setList] = useState(films)
    const [selector, setSelector] = useState([
        {name:"中国",id:1},
        {name:"米国",id:250},
        {name:"棒子国",id:94250},
        {name:"日本市",id:04250}
    ])
    useEffect(() => {
        console.log(films);
    }, [])
const onChange=(e)=>{
    console.log(e);
}
    return (
        <View className='list'>

            <View>列表</View>
            <Button onClick={() => {
                console.log(getfilms());
                setList([...getfilms()])
            }}>获取数据</Button>

            <Picker onCancel={true} range-key="name" mode='multiSelector ' range={selector} onChange={onChange}>
                <View className='picker'>
                    当前选择：{'中国'}
                </View>
            </Picker>
            {
                list ? list.map((item, index) => {
                    return (
                        <View key={index}>
                            {item.title}
                        </View>
                    )
                }) : null
            }

        </View>
    )
}
export default List
