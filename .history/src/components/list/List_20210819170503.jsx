import React, { Component, useEffect, useState } from 'react'
import { View, Button, Text, Picker } from '@tarojs/components'
const List = (props) => {
    const { films, getfilms } = props.data
    const [list, setList] = useState(films)
    const [pickerName, setPickerName] = useState("")
    const [pickerData, setPickerData] = useState("")

    const array = [
        { name: "中国", id: 1 },
        { name: "米国", id: 250 },
        { name: "棒子国", id: 251 },
        { name: "日本市", id: 252 }
    ]
    useEffect(() => {
        console.log(films);
    }, [])
    const onChange = (e) => {
        console.log(e);
        setPickerName(list[Number(e.detail.value)].name)
        setPickerData(list[Number(e.detail.value)].id)
    }
    return (
        <View className='list'>

            {/* <View>列表</View> */}
            <Button onClick={() => {
                console.log(getfilms());
                setList([...getfilms()])
            }}>获取数据</Button>
            {list?
            (
            <Picker onCancel={true} rangeKey={"original_title"}  mode='selector' range={list} onChange={onChange}>
                <View className='picker'>
                    当前选择：{pickerName}
                </View>
            </Picker>
            )
            :null}
            {/* {
                list ? list.map((item, index) => {
                    return (
                        <View key={index}>
                            {item.title}
                        </View>
                    )
                }) : null
            } */}

        </View>
    )
}
export default List
