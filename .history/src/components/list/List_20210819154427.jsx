import React, { Component, useEffect, useState } from 'react'
import { View, Button, Text } from '@tarojs/components'
const List = (props) => {
    const { films, getfilms } = props.data
    const [list ,setList] = useState(films)
    useEffect(() => {
        console.log(films);
    }, [])

    return (
        <View className='list'>

            <View>列表</View>
            <Button onClick={() => {
                console.log(getfilms());
                setList([...getfilms()])
            }}>获取数据</Button>
            {
                list ? list.map((item, index) => {
                    return (
                        <View key={index}>
                            {item.title}
                        </View>
                    )
                }) : null
            }

        </View>
    )
}
export default List
