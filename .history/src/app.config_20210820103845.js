export default {
  pages: [
    'pages/index/index',
    'pages/login/Login'
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  },
  tabBar: {
    list: [
      {
        pagePath: "pages/index/index",
        text: "首页",
        color:"red"
      },
      {
        pagePath: 'pages/login/Login',
        text: "登录",
      }
      
    ]
  }
}
