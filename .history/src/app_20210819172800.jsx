import { Component } from 'react'
import { Provider } from 'mobx-react'

import counterStore from './store/counter'
import filmStore from './store/film'

import './app.less'

const store = {
  counterStore,
  filmStore
}

class App extends Component {
  componentDidMount () {}
  config={
    pages: [
        'pages/index/index',
    
      ],
      window: {
        backgroundTextStyle: 'light',
        navigationBarBackgroundColor: '#fff',
        navigationBarTitleText: 'WeChat',
        navigationBarTextStyle: 'black'
      },
  }
  componentDidShow () {}

  componentDidHide () {}

  componentDidCatchError () {}

  // this.props.children 就是要渲染的页面
  render () {
    return (
      <Provider store={store}>
        {this.props.children}
      </Provider>
    )
  }
}

export default App
