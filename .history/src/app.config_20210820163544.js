export default {
  pages: [
      'pages/login/Login',
      'pages/index/index',
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: '',//默认标题
    navigationBarTextStyle: 'black'
  },
  
}
// 1、配置路由
// 	在app.config.js的pages中设置页面路由
// 	和小程序一样的规则
// 	若只是配置了path路径后报错
// 	则还需要添加对应的xx.config.js文件

// 2、动态跳转
// 	import Taro from '@tarojs/taro'
	
// 	Taro.navigateTo({url:'/pages/配置的剩余路径'})	保留当前页面，跳转到应用内的某个页面。但是不能跳到 tabbar 页面
// 	Taro.switchTab({url:'/pages/配置的剩余路径'}) 	 跳转到tabBar页面，并关闭其他所有非 tabBar 页面	
// 	Taro.reLaunch({url:'/pages/配置的剩余路径'}) 	 关闭所有页面，打开到应用内的某个页面
// 	Taro.redirectTo({url:'/pages/配置的剩余路径'}) 关闭当前页面，跳转到应用内的某个页面。但是不允许跳转到 tabbar 页面。
// 	Taro.navigateBack() 						关闭当前页面，返回上一页面或多级页面。可通过 getCurrentPages 获取当前的页面栈，决定需要返回几层。

// 3、动态跳转传参数
// 	Taro.navigateTo({url:'/pages/配置的剩余路径 ?参数键值对'}

// 4、接收参数
// 	import Taro,{getCurrentInstance} from '@tarojs/taro'
	
// 	getCurrentInstance().router.params.键名   h5端暂时无法获取参数
// 	Taro.Current.router.params.键名
