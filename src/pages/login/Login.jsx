import React, { Component, useEffect, useState } from 'react'
import Taro, { Config } from "@tarojs/taro"
import { AtButton, AtForm, AtInput, AtMessage, } from 'taro-ui'
import { View, Button, Text, Image } from '@tarojs/components'
import { observer, inject } from 'mobx-react'
import logo from "../../assets/log.png";
import md5 from "../../utils/md5";
import api from "../../service/api"
import './index.less'
const Login = (props) => {
    const [userName, setUser] = useState("")
    const [password, setPasswod] = useState("")
    const [platform, setPlatform] = useState("")

    const { text } = props
    const [num, setNum] = useState(0)
    useEffect(() => {
        //设置标题
        Taro.setNavigationBarTitle({
            title: "登录"
        })
        wx.login({
            success(res) {
                if (res.code) {
                    api.get("wechatmanage/ytjOpenId/findPlatformNoByCode", {
                        code: res.code,
                        address: 1
                    })
                        .then(r => {
                            if (r.data) {
                                wx.setStorageSync("openId", r.data.data.openId);
                            }
                        });
                }
            }
        });
    }, [])
    const onSubmit = (e) => {
    }
    const onReset = (e) => {
    }
    const handleChange = (value, e) => {
        value === "platform" ? setPlatform(e) : value === "username" ? setUser(e) : setPasswod(e);
    }
    const loginBtn = () => {
        if (!platform || !password || !userName) {
            wx.showToast({
                title: "请先填写登录信息！",
                icon: "none"
            });
        } else {
            let reg = /^[0-9a-zA-Z]+$/;
            if (reg.test(platform)) {
                // 存平台号
                wx.setStorageSync("platform", platform);
                wx.login({
                    success(res) {
                        if (res.code) {
                            api.post("integrated/login", {
                                code: res.code,
                                type: "H5",
                                account: userName,
                                password: md5(password),
                                address: 2
                            }).then(r => {
                                console.log(r);
                                if (r.data.code === 0) {
                                    wx.showToast({
                                        title: "登录成功",
                                        icon: "success"
                                    });
                                    wx.setStorageSync("userInfo", r.data.data);
                                    // 调绑定接口
                                    api
                                        .get("wechatmanage/ytjOpenId/binding", {
                                            openId: wx.getStorageSync("openId"),
                                            address: 1,
                                            platformNo: platform,
                                        })
                                        .then(resp => {
                                            if (resp.data.code === 0) {
                                                Taro.navigateTo({
                                                    url: "../index/index"
                                                })
                                            } else {
                                                wx.showToast({
                                                    title: resp.data.msg,
                                                    icon: "none"
                                                });
                                            }
                                        });
                                   
                                } else {
                                    wx.showToast({
                                        title: r.data.msg,
                                        icon: "none"
                                    });
                                }
                            });
                        }
                    }
                })
            } else {
                wx.showToast({
                    title: "平台号只能包含数字和字母！",
                    icon: "none"
                });
            }


        }

    }
    return (
        <View className='login'>
            <AtMessage />
            <View className="from">
                <View className="logo">
                    <Image className="logoImg" src={logo} />
                    <Text className="name">弈简运维管控平台</Text>
                </View>
                <AtInput
                    className="login_input"
                    name='platform'
                    title='平台号'
                    type='text'
                    placeholder='请输入平台号'
                    value={platform}
                    onChange={handleChange.bind(this, 'platform')}
                />
                <AtInput
                    className="login_input"
                    name='username'
                    title='账号'
                    type='text'
                    placeholder='请输入账号'
                    value={userName}
                    onChange={handleChange.bind(this, 'username')}
                />
                <AtInput
                    className="login_input"
                    name='password'
                    title='密码'
                    type='password'
                    placeholder='请输入密码'
                    value={password}
                    onChange={handleChange.bind(this, 'value')}
                />
                <AtButton onClick={loginBtn} size="small" className="login_btn">登录</AtButton>
            </View>
        </View>
    )
}
export default Login
