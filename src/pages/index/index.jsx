import { useEffect, useState } from 'react';
import { View, Button, Text, Image } from '@tarojs/components';
import Taro, { eventCenter, getCurrentInstance } from "@tarojs/taro";
import { AtButton, AtForm, AtInput,AtMessage } from 'taro-ui';
import './index.less';
import api from "../../service/api";
const Index = (props) => {
    const [key, setKey] = useState("")
    useEffect(() => {
        console.log(getCurrentPages()[getCurrentPages().length - 1].route);
        console.log(getCurrentInstance().page.$taroPath);
        console.log(getCurrentInstance().router.params.key);
        let { key } = getCurrentInstance().router.params
        console.log(key);
        wx.login({
            success(res) {
                console.log(res);
                if (res.code) {
                    wx.setStorageSync("code", res.code);
                    api
                        .get("wechatmanage/ytjOpenId/findPlatformNoByCode", {
                            code: res.code,
                            address: 1
                        }).then(r => {
                            console.log(r);
                            if (r.data.code === 1) {
                                wx.setStorageSync("openId", r.data.data.openId);
                                Taro.navigateTo({
                                    url: "../login/Login"
                                })
                            } else if (r.data.code === 0) {
                                //     获取个人操作信息
                                wx.setStorageSync("openId", r.data.data.openId);
                                // 存平台号
                                wx.setStorageSync("platform", r.data.data.platformNo);
                            }

                            if (key) {
                                wx.showModal({
                                    title: "提示",
                                    content: "是否直接开机？",
                                    success(res) {
                                        if (res.confirm) {
                                            _startComputer({}, "0", key)
                                        } else if (res.cancel) {
                                        }
                                    }
                                });
                            }
                        })
                }
            }
        })
    }, [])

    const onScan = () => {
        const _that = this;
        wx.scanCode({
            onlyFromCamera: true,
            scanType: 'qrCode',
            success(res) {
                wx.showModal({
                    title: '提示',
                    content: '确认开机？',
                    success(r) {
                        if (r.confirm) {
                            console.log(r);
                            _startComputer(res, "1");
                        } else if (r.cancel) {
                            wx.navigateBack({
                                delta: -1
                            });
                        }
                    }
                });
            },
            fail(err) {
                wx.showToast({
                    title: '操作失败!',
                    icon: 'none'
                });
            }
        });
    };
    const _startComputer = (qr_info, text, key) => {
        const userinfo = wx.getStorageSync('userInfo') || {};
        wx.showLoading({
            title: '开机中'
        });
        console.log(qr_info.path);
        console.log(qr_info.result);
        let url = qr_info.path ?
            qr_info.path.split('?')[1].split('=')[1] :
            qr_info.result ?
                qr_info.result.split('?')[1].split('=')[1] :
                ''

        api.post('integrated/control/wechatQrCodeOpen', {
            serialNumber: text === "1" ? url : key,
            address: 2
        }).then(r => {
            console.log(r);
            if (r.data.code === 0) {
                wx.hideLoading();
                wx.showToast({
                    title: '操作成功!',
                    icon: 'success'
                });
            } else {
                wx.hideLoading();

                wx.showToast({
                    title: r.data.msg,
                    icon: 'none'
                });
            }
        });
    };
    return (
        <View className='index'>
                {/* <Button className="goLogin" onClick={() => {
                Taro.navigateTo({
                    url: "../login/Login"
                })
            }}>登录</Button> */}
            <View>

        <View className="scan_wrapper"  onClick={onScan}>
          <View className="round round_1"></View>
          <View className="round round_2"></View>
          <View className="round round_3">
            <Button hover-class="hover" hover-stay-time="180" className="base_hover">
              扫码
            </Button>
          </View>
        </View>
        <View className="notice"><Text className="text">扫码一键开机</Text></View>
        </View>


                {/* <View className="round" onClick={onScan}>
                    <Button hover-class="hover" hover-stay-time="180" className="base_hover">
                        扫码
                    </Button>
                </View> */}

        </View>
    )
}
export default Index
